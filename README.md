# Echo_TCP_Client

## How to Build:

Clone this get repo and simple build using CMAKE

For Release Type or DEBUG Type build, use --DCMAKE_BUILD_TYPE=Release with cmake ..

Release tyoe build: No debugging information and full optimization

## How to Run:
Make a Server, note down its ip and port

Pass this IP and Port No as the command line argument to the main function (e.g: ./main 127.0.0.1 8081)

The client will get connected to the server

Now, write anything to the server, client will return the same message to the server.
