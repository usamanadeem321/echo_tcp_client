#include "TcpClient.h"

// constructor
TcpClient ::TcpClient() {
  _sockfd = -1;
  _port = 0;
  _server_ip_addr = "";
}

// TcpClient ::~TcpClient() { std::cout << "DESTRUCTOR called" << std::endl; }

int TcpClient::get_socket() { return _sockfd; }

int TcpClient::create_socket() {
  std::cout << "Creating Socket ..." << std::endl;
  _sockfd = socket(AF_INET, SOCK_STREAM, 0);
  if (_sockfd < 0) {
    std::cout << "Error Opening Socket!" << std::endl;
    return 0;
  }
  std::cout << "sockfd: " << _sockfd << std::endl;
  return _sockfd;
}
bool TcpClient::conn() {
  std::cout << "Connecting to server ..." << std::endl;
  // Connect to server
  if (connect(_sockfd, (struct sockaddr *)&serveraddr, sizeof(serveraddr)) <
      0) {
    perror("connect failed. Error");
    return false;
  }

  cout << "Connected\n";
  return true;
}

// a setter function to set IP address and port number.
void TcpClient::set_tcp_ip_and_port(string _server_ip_addr, int port) {
  serveraddr.sin_addr.s_addr = inet_addr(_server_ip_addr.c_str());
  serveraddr.sin_family = AF_INET;
  serveraddr.sin_port = htons(port); // htons convert byte sequence to big
                                     // endian
}

void TcpClient::continuous_receive() {
  bzero(buffer, 256);
  while (1) {
    bzero(buffer, 256);
    int a = read(_sockfd, buffer, sizeof(buffer));
    if (a < 0) {
      std::cout << "***failed reading from server***    " << a << std::endl;
    } else {
      std::cout << "From Server:  " << buffer << std::endl;
    }

    int n = send(_sockfd, buffer, sizeof(buffer), 0);
    if (n < 0) {
      std::cout << "***Error Writing to Server***" << std::endl;

    } else {
      std::cout << "Sent " << std::endl;
    }
  }
}