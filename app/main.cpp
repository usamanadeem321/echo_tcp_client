#include "TcpClient.h"

int main(int argc, char **argv) {
  if (argc < 3) {
    std::cout << "Input IP address and PORT Number" << std::endl;
    return 0;
  }
  TcpClient c1;
  c1.create_socket();
  c1.set_tcp_ip_and_port(argv[1], atoi(argv[2]));
  if (c1.conn()) {
    c1.continuous_receive();
  } else {
    std::cout << " Connection Failed! " << std::endl;
  }
}