#include <arpa/inet.h>  //inet_addr
#include <iostream>     //cout
#include <netdb.h>      //hostent
#include <stdio.h>      //printf
#include <string.h>     //strlen
#include <string>       //string
#include <sys/socket.h> //socket
#include <unistd.h>

using namespace std;

class TcpClient {
private:
  int _sockfd;
  int _port;
  std::string _server_ip_addr;
  struct sockaddr_in serveraddr;
  char buffer[256];

public:
  TcpClient();
  int create_socket();
  int get_socket();
  bool conn();
  bool send_to_server(string to_send);
  bool read_from_server();
  bool chat();
  void set_tcp_ip_and_port(string _server_ip_addr, int port);
  void continuous_receive();
};

// Create a socket with the socket() system call.
// Connect the socket to the address of the server using the connect() system
// call.